package com.example.gmo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.gmo.dto.UserRequest;
import com.example.gmo.entity.User;
import com.example.gmo.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	/**
	 * ログイン画面を表示
	 * @param model Model
	 * @return ログイン画面
	 */
	@GetMapping(value = "/user/top")
	public String top(Model model) {
		model.addAttribute("userRequest", new UserRequest());
		return "user/login";
	}

	/**
	 * ログイン認証
	 * @param userRequest リクエストデータ
	 * @param result BindingResult
	 * @param model Model
	 * @return ログイン成功画面
	 */
	@RequestMapping(value = "/user/login", params = "login", method = RequestMethod.POST)
	public String login(@Validated @ModelAttribute UserRequest userRequest, BindingResult result, Model model) {
		List<String> errorList = new ArrayList<String>();
		if (result.hasErrors()) {
			for (ObjectError error : result.getAllErrors()) {
				errorList.add(error.getDefaultMessage());
			}
			model.addAttribute("validationError", errorList);
			return "user/login";
		}
		User user = userService.getUser(userRequest.getLoginId());
		if (user == null || !user.getPassword().equals(userRequest.getPassword())) {
			errorList.add("ログインに失敗しました");
			model.addAttribute("validationError", errorList);
			return "user/login";
		}
		model.addAttribute("userlist", user);
		return "user/success";
	}

	/**
	 * アカウント新規登録画面を表示
	 * @param model Model
	 * @return アカウント新規登録画面
	 */
	@RequestMapping(value = "/user/login", params = "add", method = RequestMethod.POST)
	public String add(Model model) {
		model.addAttribute("userRequest", new UserRequest());
		return "user/add";
	}

	/**
	 * アカウント新規登録
	 * @param userRequest リクエストデータ
	 * @param result BindingResult
	 * @param model Model
	 * @return ログイン画面
	 */
	@RequestMapping(value = "/user/regist", method = RequestMethod.POST)
	public String regist(@Validated @ModelAttribute UserRequest userRequest, BindingResult result, Model model) {
		List<String> errorList = new ArrayList<String>();
		if (result.hasErrors()) {
			for (ObjectError error : result.getAllErrors()) {
				errorList.add(error.getDefaultMessage());
			}
			model.addAttribute("validationError", errorList);
			return "user/add";
		}
		boolean ret = userService.create(userRequest);
		if (!ret) {
			errorList.add(userRequest.getLoginId() + " は既に登録されています");
			model.addAttribute("validationError", errorList);
			return "user/add";
		}
		return "redirect:/user/top";
	}

}
