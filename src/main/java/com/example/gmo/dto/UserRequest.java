package com.example.gmo.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

public class UserRequest implements Serializable {

	@NotEmpty(message = "ログインIDを入力してください")
	private String loginId;

	@NotEmpty(message = "パスワードを入力してください")
	private String password;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
