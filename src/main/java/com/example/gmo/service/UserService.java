package com.example.gmo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gmo.dto.UserRequest;
import com.example.gmo.entity.User;
import com.example.gmo.repository.UserRepository;

@Service
@Transactional(rollbackOn = Exception.class)
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User getUser(String loginId) {

		//簡易プログラムなのでfindAllで全取得後に検索
		List<User> userList = userRepository.findAll();

		User user = userList.stream().filter(u -> u.getLoginId().equals(loginId)).findFirst().orElse(null);
		return user;
	}

	public boolean create(UserRequest userRequest) {

		//簡易プログラムなのでfindAllで全取得後に存在していなければ登録
		List<User> userList = userRepository.findAll();

		boolean b = userList.stream().filter(
				u -> u.getLoginId().equals(userRequest.getLoginId())).findFirst().isPresent();
		if (b) {
			return false;
		} else {
			User user = new User();
			user.setLoginId(userRequest.getLoginId());
			user.setPassword(userRequest.getPassword());
			userRepository.save(user);
			userRepository.findAll().forEach(u -> System.out.println(u));
			return true;
		}
	}
}