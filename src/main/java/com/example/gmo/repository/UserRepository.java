package com.example.gmo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.gmo.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
