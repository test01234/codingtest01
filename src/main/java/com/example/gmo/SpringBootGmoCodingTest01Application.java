package com.example.gmo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootGmoCodingTest01Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootGmoCodingTest01Application.class, args);
	}

}
